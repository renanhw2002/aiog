---
title: "Nutrição"
date: 2022-09-22T02:44:46-03:00
draft: false
---

Esta página contém informações básicas sobre alimentação visando alterações corporais:

1. Gasto calórico diário:

    Definido pelo gasto basal, isto é, o quanto de energia seu corpo gasta para manter as funções vitais funcionando corretamente, juntamente do gasto calórico proveniente das atividades físicas, tais como caminhadas, corridas leves, musculação, entre outros. Este valor é um dado importante para montar a alimentação de cada pessoa, visto que é a base para os cálculos de quanto se pode comer para ganhar ou perder peso. Para checar se gasto diário, existem calculadoras já programadas com as fórmulas estatísticas. Um exemplo é a Calorias por dia: [Calcule seu gasto calórico diário](https://caloriaspordia.com)

2. Perda de peso:

    Para perder peso é necessário estar em um déficit calórico, isto é, ingerir menos calorias do que se gasta. Muitas vezes as pessoas tendem a cortar drasticamente as calorias ingeridas, uma estratégia válida. Entretanto, esta tática costuma apresentar resultados ruins, dado que as pessoas não mantêm a alimentação tão restrita. Dito isso, uma alternativa melhor é reduzir aos poucos as calorias ingeridas, sendo recomendado um corte de 250 a 1000 kcal por dia.

3. Ganho de peso:

    Para se ganhar peso é necessário realizar o processo oposto ao anterior, ou seja, em vez de ingerir menos calorias, deve-se aumentar as quantidades, sendo o recomendado começar com um superávit pequeno e ir progredindo com o decorrer das semanas. Ex.: Começar com um superávit de 100kcal na primeira semana, 200kcal na segunda, 300kcal na terceira, ..., até que se atinja um superávit de 500kcal

4. Macro e micronutrientes

    Tratam-se da composição de cada alimento, visível na tabela nutricional de cada produto. Dito isso, os macronutrientes são aqueles cuja ingestão é relativamente alta (dezenas de gramas), sendo divididos em três: gorduras, carboidratos e proteínas. Já no caso dos micronutrientes, estes são substâncias cuja ingestão se dá em pequenas doses, como ocorre com vitaminas e sais minerais.
