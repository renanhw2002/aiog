---
title: "Divisão de treino"
date: 2021-09-18T23:27:57-03:00
draft: false
---

Divisão de treino
As divisões de treino auxiliam os praticantes a organizar seus treinos para cada grupo muscular visando uma melhor 
    distribuição para seus horários de disponibilidade

Alguns exemplos de divisão:

- Treino ABC

    | Segunda-feira 	| Terça-feira 	| Quarta-feira 	| Quinta-feira 	| Sexta-feira 	| Sábado 	|  Domingo 	|
    |:-------------:	|:-----------:	|:------------:	|:------------:	|:-----------:	|:------:	|:--------:	|
    |      Push     	|     Pull    	|     Legs     	|     Push     	|     Pull    	|  Legs  	| Descanso 	|


- Treino ABCDE

    | Segunda-feira 	| Terça-feira 	| Quarta-feira 	| Quinta-feira 	| Sexta-feira 	| Sábado 	|  Domingo 	|
    |:-------------:	|:-----------:	|:------------:	|:------------:	|:-----------:	|:------:	|:--------:	|
    |      Peito e bíceps     	|       Perna (foco em posterior)      	|     Ombro e tríceps    	|     Descanso ou Abdominal     	|     Descanso ou Cardio    	|   Costas 	| Perna (foco em quadríceps)	|
